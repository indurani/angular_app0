import {Todo} from './todo';

export const TODOS: Todo[]  = [
    {id: 11, name: 'Grocery' },
    { id: 12, name: 'Reading' },
    { id: 13, name: 'Practice Piano' },
    { id: 14, name: 'Cooking' },
    { id: 15, name: 'Meeting' },
    { id: 16, name: 'Gym' },
    { id: 17, name: 'Dinner with pals' },
    { id: 18, name: 'Walk the dog' },
    { id: 19, name: 'Finish Project' },
    { id: 20, name: 'Meditate' }
];